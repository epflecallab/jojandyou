﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrintJob
{
    public int[] indicatorValues;
    public int targetPrinter = -1;
    public int stickerNumber = -1;

    

    public PrintJob(int[] values, string target, int _stickerNumber)
    {
        if (target == "TABLET") targetPrinter = 1;
        if (target == "TV") targetPrinter = 2;

        stickerNumber = _stickerNumber;


        indicatorValues = new int[values.Length];

        for(int i = 0; i < indicatorValues.Length; i++)
        {
            if(values[i] == -1)
            {
                indicatorValues[i] = -1;
                continue;
            }

            float v = map(values[i], 0, 100, 0, 7);

            indicatorValues[i] = Mathf.RoundToInt(Mathf.Floor(v));

            if (indicatorValues[i] == 7) indicatorValues[i] = 6;
        }

        indicatorValues = ReorderBasedOnFlyerOrder(indicatorValues);
    }

    float map(float s, float a1, float a2, float b1, float b2)
    {
        return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
    }

    private int[] ReorderBasedOnFlyerOrder(int[] v)
    {
        int[] flyerClipOrder = { 4, 0, 2, 1, 3 };

        int[] v_old = v;
        int[] v_new = new int[v.Length];

        for (int i = 0; i < v_new.Length; i++)
        {
            v_new[i] = v_old[flyerClipOrder[i]];
        }
        return v_new;

    }
}