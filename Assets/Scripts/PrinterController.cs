﻿// Saves screenshot as PNG file.
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.IO;
using System.Collections.Generic;

public class PrinterController : MonoBehaviour
{
    int id;
    public Camera flyerCamera;

    public bool debugMode = false;

    public List<PrintJob> printQue = new List<PrintJob>();

    float timerForNextPrint = 0;
    float delayInbetweenPrintJobs = 2f;

    public GameObject upLayer;
    public GameObject downLayer;

    public RectTransform[] upIndicators;
    public RectTransform[] downIndicators;


    string projectFoldePath;
    bool useDebugEditorRelativePaths;

    void Start()
    {
        projectFoldePath = GameObject.Find("Manager").GetComponent<GeneralManager>().projectFoldePath;
        useDebugEditorRelativePaths = GameObject.Find("Manager").GetComponent<GeneralManager>().useDebugEditorRelativePaths;

        if (useDebugEditorRelativePaths)
        {
            projectFoldePath = "../../";
        }

        id = PlayerPrefs.GetInt("id_counter");
    }

    void Update()
    {
        ExpeditePrintingJobs();
    }


    public void SendToPrint(PrintJob job)
    {
        printQue.Add(job);
    }

    void ExpeditePrintingJobs()
    {
        if (printQue.Count > 0)
        {
            timerForNextPrint -= Time.deltaTime;

            if (timerForNextPrint < 0)
            {
                timerForNextPrint = delayInbetweenPrintJobs;

                ExecutePrintJob(printQue[0]);
                printQue.RemoveAt(0);
            }
        }
    }

    void ExecutePrintJob(PrintJob job)
    {
        SetIndicators(job.stickerNumber, job.indicatorValues);

        GenerateFileToPrint(job.targetPrinter);
    }

    void SetIndicators(int layout, int[] values)
    {
        upLayer.SetActive(false);
        downLayer.SetActive(false);
        RectTransform[] indicators;

        int offset = -90;

        if (layout == 0)
        {
            upLayer.SetActive(true);
            indicators = upIndicators;
        }
        else //(layout == 1)
        {
            downLayer.SetActive(true);
            indicators = downIndicators;
        }

        for(int i = 0; i < indicators.Length; i++)
        {
            indicators[i].gameObject.SetActive(true);

            if(values[i] == -1)
            {
                indicators[i].gameObject.SetActive(false);
                continue;
            }

            indicators[i].rotation = Quaternion.Euler(new Vector3(0, 0, offset - (180) / 6 * values[i]));
        }
    }

    public void GenerateFileToPrint(int target) {


        StartCoroutine(UploadPNG(target));

    }

    IEnumerator UploadPNG(int target)
    {
        // We should only read the screen buffer after rendering is complete
        yield return new WaitForEndOfFrame();

        // Create a texture the size of the screen, RGB24 format
        int width = 213;
        int height = 1004;
        Texture2D tempTex = new Texture2D(width, height, TextureFormat.RGB24, false);


        RenderTexture.active = flyerCamera.targetTexture;
        flyerCamera.Render();


        // Read screen contents into the texture
        tempTex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        tempTex.Apply();

        // Encode texture into PNG
        byte[] bytes = tempTex.EncodeToPNG();

        //string v_path = Application.streamingAssetsPath;
        string v_targerfolderPath = projectFoldePath + "Final_App/Desktop_Apps/PrinterSystem/Printer" + target;
        string v_targerfolderPath_Debug = projectFoldePath + "Projects/builds/Debugging_Stickers_print/Printer" + target;
        if (!Directory.Exists(v_targerfolderPath))
        {
            Directory.CreateDirectory(v_targerfolderPath);
        }
        if (!Directory.Exists(v_targerfolderPath_Debug))
        {
            Directory.CreateDirectory(v_targerfolderPath_Debug);
        }
        //Save file with an unique name
        if (debugMode)   System.IO.File.WriteAllBytes(v_targerfolderPath_Debug + "/card_" + id.ToString() + ".png", bytes);
        else            System.IO.File.WriteAllBytes(v_targerfolderPath + "/card_" + id.ToString() + ".png", bytes);
        id++;
        PlayerPrefs.SetInt("id_counter", id);

        Object.Destroy(tempTex);


    }


    

}



