﻿using System.Collections;
using System.Collections.Generic;
using extOSC;
using UnityEngine;

public class ExperienceManager : MonoBehaviour
{

    OSCTransmitter oscTransmitter_interface;
    OSCTransmitter oscTransmitter_videoplayer;
    OSCReceiver oscReciver;

    int state = -1;

    // -1 waiting to start
    // 0 initial question
    // 1 clip1
    // 2 clip2
    // 3 clip3
    // 4 clip4
    // 5 clip5
    // 6 final message

    public int[] clip_order = new int[5];

    public DatabaseDataType data;// = new DatabaseDataType("a", "a", "a", new int[5][]);

    public string setup_type; // "TV" "TABLET"

    public GeneralManager manager;

    public StateBarLogic stateBar;

    public GameObject deactivationLabel;

    public DataHolder dataHolder;

    bool preventNextSave;

    void Start()
    {
        preventNextSave = false;

        oscReciver = GetComponent<OSCReceiver>();
        oscTransmitter_interface = gameObject.GetComponents<OSCTransmitter>()[0];
        oscTransmitter_videoplayer = gameObject.GetComponents<OSCTransmitter>()[1];


        oscReciver.Bind("/AskVideoOrder/", StartVideoExperience);
        oscReciver.Bind("/VideoEnded/", VideoEnded);
        oscReciver.Bind("/VideoQuestionDone/", VideoQuestionDone);
        oscReciver.Bind("/SequenceEnded/", LaunchEndPanel);
        oscReciver.Bind("/AnswersResult/", EndExperience);
        oscReciver.Bind("/FirstClick/", UserFirstInput);


        RestartExperience();
    }

    void RestartExperience()
    {
        ResetInterfaceAndPlayer();

        state = -1;

        stateBar.UpdateState(state);
    }

    public void ForceRestart() {

        print("Restarting");

        state = -1;

        stateBar.UpdateState(state);

        var message = new OSCMessage("/ForceRestart/");
        message.AddValue(OSCValue.String(""));

        oscTransmitter_interface.Send(message);


        message = new OSCMessage("/ForceRestart/");
        message.AddValue(OSCValue.String(""));

        oscTransmitter_videoplayer.Send(message);

    }

    public void ResetInterfaceAndPlayer()
    {
        var message = new OSCMessage("/Reset/");
        message.AddValue(OSCValue.String(""));

        oscTransmitter_interface.Send(message);


        message = new OSCMessage("/Reset/");
        message.AddValue(OSCValue.String(""));

        oscTransmitter_videoplayer.Send(message);
    }

    public void SetDebugMode(bool _state)
    {

        OSCMessage message = new OSCMessage("/DebugMode/");
        message.AddValue(OSCValue.Bool(_state));

        oscTransmitter_videoplayer.Send(message);
    }
    public void DeactivateNextSession(bool _state)
    {
        if (_state)
        {
            if (!preventNextSave)
            {
                preventNextSave = true;
                deactivationLabel.SetActive(true);
            }
            else
            {
                preventNextSave = false;
                deactivationLabel.SetActive(false);
            }
        }
        else {
            preventNextSave = false;
            deactivationLabel.SetActive(false);
        }
    }



    //////////////////////////////////////////////// OSC MESSAGE HANDLERS
    ///
    void UserFirstInput(OSCMessage message)
    {
        state = 0;
        stateBar.UpdateState(state);
    }

    void StartVideoExperience(OSCMessage mes)
    {
        state = 1;
        stateBar.UpdateState(state);

        GenerateVideoOrderAndPlay(mes);
    }

    void GenerateVideoOrderAndPlay(OSCMessage mes)
    {
        clip_order = new int[] { 0, 1, 2, 3, 4 };
        int temp_int;

        for (int i = 0; i < clip_order.Length; i++)
        {
            int rnd = Random.Range(0, clip_order.Length);
            temp_int = clip_order[rnd];
            clip_order[rnd] = clip_order[i];
            clip_order[i] = temp_int;
        }
        string str = "";

        for (int i = 0; i < clip_order.Length; i++)
        {
            str += clip_order[i].ToString();
        }
        var message = new OSCMessage("/VideoOrderAndPlay/");

    


        message.AddValue(OSCValue.String(str));

        string language = mes.Values[0].StringValue;
        print(language);
        if (language == "FR") message.AddValue(OSCValue.Bool(true));
        else message.AddValue(OSCValue.Bool(false));

        oscTransmitter_videoplayer.Send(message);
    }

    void VideoEnded(OSCMessage message)
    {
        message = new OSCMessage("/AskQuestion/");
        message.AddValue(OSCValue.String(""));

        oscTransmitter_interface.Send(message);
    }

    void VideoQuestionDone(OSCMessage message)
    {
        state++;
        stateBar.UpdateState(state);

        message = new OSCMessage("/Play/");
        message.AddValue(OSCValue.String(""));

        oscTransmitter_videoplayer.Send(message);
    }

    void LaunchEndPanel(OSCMessage message)
    {
        message = new OSCMessage("/ShowEndPanel/");
        message.AddValue(OSCValue.String(""));

        oscTransmitter_interface.Send(message);
    }

    void EndExperience(OSCMessage message)
    {
        RestartExperience();
        if (!preventNextSave) SaveData(message);
        else DeactivateNextSession(false);
    }

    void SaveData(OSCMessage message)
    {
        string language = message.Values[0].StringValue;
        string region   = message.Values[1].StringValue;
        string sex      = message.Values[2].StringValue;
        string age      = message.Values[3].StringValue;

        int[][] shuffle_answers = new int[5][];

        shuffle_answers[0] = new int[] { message.Values[4].IntValue,  message.Values[5].IntValue,  message.Values[6].IntValue };
        shuffle_answers[1] = new int[] { message.Values[7].IntValue,  message.Values[8].IntValue,  message.Values[9].IntValue };
        shuffle_answers[2] = new int[] { message.Values[10].IntValue, message.Values[11].IntValue, message.Values[12].IntValue };
        shuffle_answers[3] = new int[] { message.Values[13].IntValue, message.Values[14].IntValue, message.Values[15].IntValue };
        shuffle_answers[4] = new int[] { message.Values[16].IntValue, message.Values[17].IntValue, message.Values[18].IntValue };

        int[][] answers = new int[5][];

        answers[0] = shuffle_answers[System.Array.IndexOf(clip_order, 0)];
        answers[1] = shuffle_answers[System.Array.IndexOf(clip_order, 1)];
        answers[2] = shuffle_answers[System.Array.IndexOf(clip_order, 2)];
        answers[3] = shuffle_answers[System.Array.IndexOf(clip_order, 3)];
        answers[4] = shuffle_answers[System.Array.IndexOf(clip_order, 4)];  

        data = new DatabaseDataType(setup_type, language, region, sex, age, answers);

        dataHolder.UpdateAnswer(data);

        manager.SaveAndPrintEntry(data);

    }
}
