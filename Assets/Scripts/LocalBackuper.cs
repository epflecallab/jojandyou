﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEditor;
using System;

public class LocalBackuper : MonoBehaviour
{
   
    string projectFoldePath;
    bool useDebugEditorRelativePaths;

    private void Start()
    {
        projectFoldePath = GameObject.Find("Manager").GetComponent<GeneralManager>().projectFoldePath;
        useDebugEditorRelativePaths = GameObject.Find("Manager").GetComponent<GeneralManager>().useDebugEditorRelativePaths;


        if (useDebugEditorRelativePaths)
        {
            projectFoldePath = "../../";
        }
    }
    public void SaveData(DatabaseDataType data)
    {

        string entry = "";

        entry += data.TimeStamp;
        entry += ",";

        entry += data.Day;
        entry += ",";

        entry += data.Setup;
        entry += ",";

        entry += data.Language;
        entry += ",";

        entry += data.Region;
        entry += ",";

        entry += data.Sex;
        entry += ",";

        entry += data.Age;
        entry += ",";

        entry += data.clip1_violence;
        entry += ",";
        entry += data.clip1_acceptance;
        entry += ",";
        entry += data.clip1_action;
        entry += ",";

        entry += data.clip2_violence;
        entry += ",";
        entry += data.clip2_acceptance;
        entry += ",";
        entry += data.clip2_action;
        entry += ",";

        entry += data.clip3_violence;
        entry += ",";
        entry += data.clip3_acceptance;
        entry += ",";
        entry += data.clip3_action;
        entry += ",";

        entry += data.clip4_violence;
        entry += ",";
        entry += data.clip4_acceptance;
        entry += ",";
        entry += data.clip4_action;
        entry += ",";

        entry += data.clip5_violence;
        entry += ",";
        entry += data.clip5_acceptance;
        entry += ",";
        entry += data.clip5_action;

        string v_backupFileName = projectFoldePath + "Final_App/Desktop_Apps/ParticipantsAnswers/" + "ParticipantsData.csv";

        using (StreamWriter writetext = File.AppendText(v_backupFileName))
        {
            writetext.Write(entry + "\n");
            writetext.Close();
        }
        try
        {
            v_backupFileName = "/Volumes/DATA_BACKUP/" + "ParticipantsData_BACKUP.csv";
            using (StreamWriter writetext = File.AppendText(v_backupFileName))
            {
                writetext.Write(entry + "\n");
                writetext.Close();
            }
        }
        catch (Exception e){
            print(e);
            print("No SD card for backup has been found. The entry has nort been backed up.");
        }
        
    }
}
