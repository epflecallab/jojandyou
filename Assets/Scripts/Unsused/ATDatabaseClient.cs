﻿using System.Collections;
using System.Collections.Generic;
using AirtableUnity.PX.Model;
using UnityEngine;

public class ATDatabaseClient : MonoBehaviour
{
    

[Header("Environment Configuration")]
    public string ApiVersion;
    public string AppKey;
    public string ApiKey;
    public string TableName;

    // Start is called before the first frame update
    void Start()
    {
        AirtableUnity.PX.Proxy.SetEnvironment(ApiVersion, AppKey, ApiKey);
    }

    public void SaveData(DatabaseDataType data)
    {
        string entryString = JsonUtility.ToJson(data);
        entryString.Remove(0, 1);
        entryString = "{\"fields\":" + entryString + "}"; 

        print(entryString);
        StartCoroutine(CallRecordCreation(entryString));
    }

    private IEnumerator CallRecordCreation(string entryString)
    {
        yield return StartCoroutine(AirtableUnity.PX.Proxy.CreateRecordCo<BaseField>(TableName, entryString, OnResponseFinish));
    }


    private void OnResponseFinish(BaseRecord<BaseField> record)
    {
        var msg = "record id: " + record?.id + "\n";
        msg += "field id: " + record?.fields?.Id + "\n";
        msg += "created at: " + record?.createdTime?.ToString();

        Debug.Log("[Airtable Unity] - Create Record: " + "\n" + msg);
    }

    public static string GetTime(string type)
    {
        System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);

        if (type == "UTC")
        {
            return ((int)(System.DateTime.UtcNow - epochStart).TotalSeconds).ToString();
        }
        else
        {
            return System.DateTime.Now.ToString("yyyy-MM-dd");
        }
    }

    float SetPrecision(float f)
    {
        return Mathf.Round(f * 10f) / 10f;
    }
}



