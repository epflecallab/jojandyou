﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataCollector : MonoBehaviour
{
    [Header("Base data")]

    public string language;
    public string sex;
    public string region;
    public string age;

    [Header("Experience data")]

    public float[] clip1 = new float[3];
    public float[] clip2 = new float[3];
    public float[] clip3 = new float[3];
    public float[] clip4 = new float[3];
    public float[] clip5 = new float[3];

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
