﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataHolder : MonoBehaviour
{
    public DatabaseDataType answers;

    public GameObject dataDysplay;

    public Image[] indicators1;
    public Image[] indicators2;

    public GeneralManager manager;

    public void DisplayAnswer(bool _state)
    {
        dataDysplay.SetActive(_state);
    }

    public void UpdateAnswer(DatabaseDataType d)
    {
        answers = d;


        int[] violence_values = {
            answers.clip1_violence,
            answers.clip2_violence,
            answers.clip3_violence,
            answers.clip4_violence,
            answers.clip5_violence
        };

        int[] action_values = {
            answers.clip1_action,
            answers.clip2_action,
            answers.clip3_action,
            answers.clip4_action,
            answers.clip5_action
        };

        
        // using the print job class to map and sort the values... it's a dirty shortcut
        PrintJob job1 = new PrintJob(violence_values, "", 0);
        PrintJob job2 = new PrintJob(action_values, "", 0);

        violence_values = job1.indicatorValues;
        action_values = job2.indicatorValues;

        for(int i = 0; i < indicators1.Length; i++)
        {
            indicators1[i].gameObject.SetActive(false);
            indicators2[i].gameObject.SetActive(false);
        }
       

        for (int i = 0; i < indicators1.Length; i++){
            if (violence_values[i] != -1) indicators1[i].gameObject.SetActive(true); 
            indicators1[i].GetComponent<RectTransform>().rotation = Quaternion.Euler(new Vector3(0, 0,  (-1) * (180 / 6) * violence_values[i]));
        }

        for (int i = 0; i < indicators2.Length; i++)
        {
            if (action_values[i] != -1) indicators2[i].gameObject.SetActive(true);
            indicators2[i].GetComponent<RectTransform>().rotation = Quaternion.Euler(new Vector3(0, 0, (-1) * (180 / 6) * action_values[i]));
        }

    }

 
    public void Reprint()
    {
        manager.Print(answers);
    }
}
