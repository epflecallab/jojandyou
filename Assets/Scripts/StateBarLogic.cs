﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateBarLogic : MonoBehaviour
{

    public GameObject[] bar_indicators;


    public void UpdateState(int state)
	{
        for(int i = 0; i < bar_indicators.Length; i++)
		{
            if (state > i - 1) bar_indicators[i].SetActive(true);
            else bar_indicators[i].SetActive(false);
        }

	}



}
