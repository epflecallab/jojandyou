﻿using System.Collections;
using System.Collections.Generic;
using extOSC;
using UnityEngine;

public class GeneralManager : MonoBehaviour
{
    ATDatabaseClient atClient;
    LocalBackuper localBackuper;

    public ExperienceManager tabletExperience;
    public ExperienceManager tvExperience;

    PrinterController printerController;

    public bool UseVortexPath;
    public string projectFolderPath_Vortex;
    public string projectFolderPath_StMoritz;

    public bool useDebugEditorRelativePaths;

    [HideInInspector]
    public string projectFoldePath;

    bool debugModeActive = false;

    public bool debug_IP_adresses = false;

    private void Awake()
    {
        if (debug_IP_adresses)
        {
            OSCTransmitter tabletExperience_transmitter_interface = tabletExperience.gameObject.GetComponents<OSCTransmitter>()[0];
            OSCTransmitter tabletExperience_transmitter_videoplayer = tabletExperience.gameObject.GetComponents<OSCTransmitter>()[1];

            OSCTransmitter tvExperience_transmitter_interface = tvExperience.gameObject.GetComponents<OSCTransmitter>()[0];
            OSCTransmitter tvExperience_transmitter_videoplayer = tvExperience.gameObject.GetComponents<OSCTransmitter>()[1];

            tabletExperience_transmitter_interface.RemoteHost = "192.168.0.100";
            tabletExperience_transmitter_videoplayer.RemoteHost = "192.168.0.100";

            tvExperience_transmitter_interface.RemoteHost = "192.168.0.100";
            tvExperience_transmitter_videoplayer.RemoteHost = "192.168.0.100";
        }
    }

    void Start()
    {

        if (UseVortexPath) projectFoldePath = projectFolderPath_Vortex;
        else projectFoldePath = projectFolderPath_StMoritz;

        atClient = GetComponent<ATDatabaseClient>();
        localBackuper = GetComponent<LocalBackuper>();

        printerController = GameObject.Find("Printer").GetComponent<PrinterController>();

        //StartCoroutine(TEST_PRINT(2000));
    }

    public void SetDebugMode(bool state)
    {
        debugModeActive = state;

        printerController.debugMode = state;

        if (tabletExperience != null) tabletExperience.SetDebugMode(state);
        
        if (tvExperience != null) tvExperience.SetDebugMode(state);
        
    }

    public void SaveAndPrintEntry(DatabaseDataType data)
    {
        Debug_print_data(data);

        if(!debugModeActive) localBackuper.SaveData(data);

        // The printer controller is launched even if we are in debug mode
        // as it will save the print-pngs on an alternative folder
        Print(data);


        //The live save on the onlyine airtable database is deactivated as it
        //requires a stable internet connection.
        //atClient.SaveData(data);

    }


    public void Print(DatabaseDataType data)
    {
        PrintJob job = new PrintJob(new int[]{
            data.clip1_violence,
            data.clip2_violence,
            data.clip3_violence,
            data.clip4_violence,
            data.clip5_violence
        }, data.Setup, 0);

        printerController.SendToPrint(job);

        job = new PrintJob(new int[]{
            data.clip1_action,
            data.clip2_action,
            data.clip3_action,
            data.clip4_action,
            data.clip5_action
        }, data.Setup, 1);

        printerController.SendToPrint(job);
    }


    IEnumerator TEST_PRINT(int delay)
    {
        yield return new WaitForSeconds(delay / 1000);
        
        PrintJob job = new PrintJob(new int[]{
            Mathf.RoundToInt(Random.Range(0, 100)),
            Mathf.RoundToInt(Random.Range(0, 100)),
            Mathf.RoundToInt(Random.Range(0, 100)),
            Mathf.RoundToInt(Random.Range(0, 100)),
            Mathf.RoundToInt(Random.Range(0, 100))
        }, "TABLET", 0);

        printerController.SendToPrint(job);

        job = new PrintJob(new int[]{
            Mathf.RoundToInt(Random.Range(50, 100)),
            Mathf.RoundToInt(Random.Range(50, 100)),
            Mathf.RoundToInt(Random.Range(50, 100)),
            Mathf.RoundToInt(Random.Range(50, 100)),
            Mathf.RoundToInt(Random.Range(50, 100))
        }, "TABLET", 1);

        printerController.SendToPrint(job);


        job = new PrintJob(new int[]{
            Mathf.RoundToInt(Random.Range(0, 100)),
            Mathf.RoundToInt(Random.Range(0, 100)),
            Mathf.RoundToInt(Random.Range(0, 100)),
            -1,
            -1
        }, "TV", 0);

        printerController.SendToPrint(job);

        job = new PrintJob(new int[]{
            Mathf.RoundToInt(Random.Range(50, 100)),
            Mathf.RoundToInt(Random.Range(50, 100)),
            Mathf.RoundToInt(Random.Range(50, 100)),
            Mathf.RoundToInt(Random.Range(50, 100)),
            -1,
        }, "TV", 1);

        printerController.SendToPrint(job);
    }


    //for debug, it prints a DatabaseDataType variable in a readable way
    void Debug_print_data(DatabaseDataType data)
    {
        string s = "{";
        s += data.clip1_violence + ", ";
        s += data.clip1_acceptance + ", ";
        s += data.clip1_action + "}  {";

        s += data.clip2_violence + ", ";
        s += data.clip2_acceptance + ", ";
        s += data.clip2_action + "}  {";

        s += data.clip3_violence + ", ";
        s += data.clip3_acceptance + ", ";
        s += data.clip3_action + "}  {";

        s += data.clip4_violence + ", ";
        s += data.clip4_acceptance + ", ";
        s += data.clip4_action + "}  {";

        s += data.clip5_violence + ", ";
        s += data.clip5_acceptance + ", ";
        s += data.clip5_action + "}";

        print(s);
    }

}
