﻿using UnityEngine;
using System.Collections;
using System;



    public class DatabaseDataType
    {
        public int TimeStamp;       // epoch utc: 234354657
        public string Day;          // 11.30.2020 12:40

        public string Region;    
        public string Sex;          // M, F
        public string Language;     //FR, EN
        public string Age;          //14, 15, 16, 17, +18

        public string Setup;        // TV, TABLET

        public int clip1_violence;
        public int clip1_acceptance;
        public int clip1_action;

        public int clip2_violence;
        public int clip2_acceptance;
        public int clip2_action;

        public int clip3_violence;
        public int clip3_acceptance;
        public int clip3_action;

        public int clip4_violence;
        public int clip4_acceptance;
        public int clip4_action;

        public int clip5_violence;
        public int clip5_acceptance;
        public int clip5_action;

        public DatabaseDataType(string _setup, string _language, string _region, string _sex, string _age, int[][] _answers)
        {

            System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);

            TimeStamp = (int)(System.DateTime.UtcNow - epochStart).TotalSeconds;

            Day = DateTime.UtcNow.ToString();

            Sex = _sex;
            Age = _age;
            Language = _language;
            Region = _region;
            Setup = _setup;

            clip1_violence = _answers[0][0];
            clip1_acceptance = _answers[0][1];
            clip1_action = _answers[0][2];

            clip2_violence = _answers[1][0];
            clip2_acceptance = _answers[1][1];
            clip2_action = _answers[1][2];

            clip3_violence = _answers[2][0];
            clip3_acceptance = _answers[2][1];
            clip3_action = _answers[2][2];

            clip4_violence = _answers[3][0];
            clip4_acceptance = _answers[3][1];
            clip4_action = _answers[3][2];

            clip5_violence = _answers[4][0];
            clip5_acceptance = _answers[4][1];
            clip5_action = _answers[4][2];
        }

    }

