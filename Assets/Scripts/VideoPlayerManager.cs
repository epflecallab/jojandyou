﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using extOSC;
using UnityEngine.Video;

public class VideoPlayerManager : MonoBehaviour
{

    OSCTransmitter oscTransmitter;
    OSCReceiver oscReciver;

    public int clip_index;
    int[] clip_order = new int[5];

    public VideoClip[] videoClips_ENG;
    public VideoClip[] videoClips_FR;
    public VideoClip[] videoClips_DEBUG;

    VideoPlayer videoPlayer;

    bool debugMode;
    bool frenchSubs;

    void Start()
    {

        frenchSubs = false;
        debugMode = false;

        videoPlayer = GetComponent<VideoPlayer>();
        videoPlayer.loopPointReached += VideoEnded;

        clip_index = -1;

        oscTransmitter = GetComponent<OSCTransmitter>();
        oscReciver = GetComponent<OSCReceiver>();

        oscReciver.Bind("/VideoOrderAndPlay/", RecivedVideoOrder);
        oscReciver.Bind("/Reset/", Reset);
        oscReciver.Bind("/DebugMode/", ChangeDebugMode);
        
        oscReciver.Bind("/Play/", RecivedPlay);

    }
    public void PlayClip()
    {
        clip_index++;
        if (clip_index == 5) // length of total clips
        {

            print("Sequence Ended");
            var message = new OSCMessage("/SequenceEnded/");
            message.AddValue(OSCValue.String(""));

            oscTransmitter.Send(message);

            clip_index = -1;
        }

        else
        {
            VideoClip[] clipList;

            if (debugMode) clipList = videoClips_DEBUG;
            else
            {
                if (frenchSubs) clipList = videoClips_FR;
                else clipList = videoClips_ENG;
            }

            videoPlayer.clip = clipList[clip_order[clip_index]];
            videoPlayer.Play();
        }
    }

    public void SetFrenchSubs(bool state)
    {
        frenchSubs = state;
    }


    ///////////// OSC METHOD HANDLERS
    private void Reset(OSCMessage message)
    {
        clip_index = -1;
        clip_order = new int[5];

        frenchSubs = false;

    }
    private void RecivedPlay(OSCMessage message)
    {
        PlayClip();
    }
    private void RecivedVideoOrder(OSCMessage message)
    {
        string mes = message.Values[0].StringValue;

        for (int i = 0; i < mes.Length; i++)
        {
            clip_order[i] = int.Parse(mes[i].ToString());
        }
        PlayClip();
    }
    void ChangeDebugMode(OSCMessage message)
    {
        print("player in debug");
        debugMode = message.Values[0].BoolValue;
    }

    
    
 


    //////////// VIDEO EVENTS


    void VideoEnded(UnityEngine.Video.VideoPlayer vp)
    {

        var message = new OSCMessage("/VideoEnded/");
        message.AddValue(OSCValue.String(""));

        oscTransmitter.Send(message);
    }
}
