﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class OptionMenuController : MonoBehaviour
{
    public bool hideRightExperienceControls;

    public GeneralManager generalManager;

    public ExperienceManager tabletExperience;
    public ExperienceManager tvExperience;

    public DataHolder tabletData;
    public DataHolder tvData;

    public GameObject[] buttonList;

    public GameObject labelLeft;
    public GameObject labelRight;
    public Color unactiveText;
    public Color unactiveBg;
    public Color activeText;
    public Color activeBg;

    int currentSelection = 0;

    float autoCloseTImer = 0;
    bool open = false;

    GameObject content;
    GameObject debugWarning;

    KeyCode[] validKeys = {
        KeyCode.LeftArrow,
        KeyCode.RightArrow,
        KeyCode.UpArrow,
        KeyCode.DownArrow,
        KeyCode.Return,
        KeyCode.Escape,
        KeyCode.D,
    };

    KeyCode currentKey;



    void Start()
    {
        if (hideRightExperienceControls)
        {
            buttonList[4].SetActive(false);
            buttonList[5].SetActive(false);
            buttonList[6].SetActive(false);
            buttonList[7].SetActive(false);

            labelRight.SetActive(false);

            labelLeft.GetComponent<TextMeshProUGUI>().text = "EPERIENCE OPTIONS";
            labelLeft.transform.GetChild(0).gameObject.SetActive(false);
        }

        content = transform.GetChild(0).gameObject;
        debugWarning = transform.GetChild(1).gameObject;

        DeselectAll();
        Select(0);
    }

    void Update()
    {

        if (CheckForValidKeyUp())
        {
            OpenMenu();

            if (currentKey == KeyCode.LeftArrow && !hideRightExperienceControls) currentSelection -= 4;
            if (currentKey == KeyCode.RightArrow && !hideRightExperienceControls) currentSelection += 4;
            if (currentKey == KeyCode.UpArrow) currentSelection -= 1;
            if (currentKey == KeyCode.DownArrow) currentSelection += 1;

            if (IsArrowKey(currentKey))
            {
                BoundCurrentSelection();

                DeselectAll();
                Select(currentSelection);
            }

            if(currentKey == KeyCode.D)
            {
                if (debugWarning.activeInHierarchy)
                {
                    debugWarning.SetActive(false);
                    generalManager.SetDebugMode(false);
                }
                else
                {
                    debugWarning.SetActive(true);
                    generalManager.SetDebugMode(true);
                }
            }

            if(currentKey == KeyCode.Return)
            {
             
                if (currentSelection == 0 && tabletExperience != null) tabletExperience.DeactivateNextSession(true);

                else if (currentSelection == 4 && tvExperience != null) tvExperience.DeactivateNextSession(true);

                else if (currentSelection == 1) tabletData.Reprint();

                else if (currentSelection == 5) tvData.Reprint();

                else if (currentSelection == 2) tabletData.DisplayAnswer(true);

                else if (currentSelection == 6) tvData.DisplayAnswer(true);

                else if (currentSelection == 3) tabletExperience.ForceRestart();

                else if (currentSelection == 7) tvExperience.ForceRestart();



                if (open) CloseMenu();
            }

            if(currentKey == KeyCode.Escape) CloseMenu();
        }

        if (open) TickAutoCloseTimer();
    
    }












    

    void OpenMenu()
    {
        open = true;
        autoCloseTImer = 5.0f;
        content.SetActive(true);

        tabletData.DisplayAnswer(false);
        tvData.DisplayAnswer(false);
    }

    void CloseMenu()
    {
        content.SetActive(false);
        autoCloseTImer = 0;
        open = false;
    }

    void TickAutoCloseTimer()
    {
        autoCloseTImer -= Time.deltaTime;
        if (autoCloseTImer < 0)
        {
            CloseMenu();
        }
    }

    void DeselectAll()
    {
        for(int i = 0; i < buttonList.Length; i++)
        {
            Image bg = buttonList[i].GetComponent<Image>();
            TextMeshProUGUI text = buttonList[i].transform.GetChild(0).GetComponent<TextMeshProUGUI>();

            bg.color = unactiveBg;
            text.color = unactiveText;
        }
    }

    void Select(int target)
    {
        Image bg = buttonList[target].GetComponent<Image>();
        TextMeshProUGUI text = buttonList[target].transform.GetChild(0).GetComponent<TextMeshProUGUI>();

        bg.color = activeBg;
        text.color = activeText;
    }

    void BoundCurrentSelection()
    {
        if (currentSelection < 0) currentSelection = buttonList.Length + currentSelection;

        if (!hideRightExperienceControls) currentSelection %= buttonList.Length;
        else currentSelection %= buttonList.Length/2;
    }

    bool CheckForValidKeyUp()
    {
        for(int i = 0; i < validKeys.Length; i++)
        {
            if (Input.GetKeyUp(validKeys[i]))
            {
                currentKey = validKeys[i];
                return true;
            }
        }
        return false;
    }

    bool IsArrowKey(KeyCode k)
    {
        if (k == KeyCode.LeftArrow) return true;
        if (k == KeyCode.RightArrow) return true;
        if (k == KeyCode.UpArrow) return true;
        if (k == KeyCode.DownArrow) return true;
        return false;
    }

}
